# Konfiguracja Keycloak

Należy pobrać  instalację [Keycloak](http://sourceforge.net/projects/keycloak/files/), 
wersję 1.2.0.Final, rozpakować archiwum, przejść do katalogu `bin` i uruchomić `standalone.bat` (lub `.sh` :) ).

Następnie należy przejść do [konsoli administracyjnej](http://localhost:8080/auth/admin/index.html), i zalogować się, użytkownik `admin` i hasło `admin`.

Zaraz po zalogowaniu zostaniemy poproszeni o zmianę hasła.

W następnym kroku należy skonfigurować zasób (resource) (aplikację którą chcemy chronić).
Aby skonfigurować zasób należy przejść Clients > Create, następnie podać Client ID, oraz skonfigurować poprawne URL z których aplikacja będzie przekierowywana do SSO.

# Przygotowanie aplikacji Spring Boot

Należy otworzyć plik `src/main/resources/application.properties` i skonfigurować parametry dostępu do SSO.

	keycloak.realmKey = 
 
 Który należy skopiować z Settings > Keys > Public key
 
 Następnie ustawić nazwę zasobu,
 	
 	keycloak.resource = <wybrana nazwa zasobu>
oraz hasło,

	keycloak.credentials.secret = 
	
który można znaleźć, Clients > <nazwa zasobu> > Credentials

A potem już tylko,

	mvn package
	java -jar target/infinite-keycloak-0.0.1-SNAPSHOT.jar
	
i przechodzimy aplikacj, http://localhost:7777/hello.

Miłej zabawy!!!

 	
