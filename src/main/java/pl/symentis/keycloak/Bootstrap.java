package pl.symentis.keycloak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Bootstrap {

	@Bean
	public ServletRegistrationBean helloServlet() {
		ServletRegistrationBean registrationBean = new ServletRegistrationBean(new HelloServlet());
		registrationBean.addUrlMappings("/hello");
		return registrationBean;
	}

	public static void main(String[] args) {
		SpringApplication.run(Bootstrap.class, args);
	}

}